const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
const IMAGENES = document.querySelectorAll('#targetas article.carousel ');
const $imagen = document.querySelector('#targetas');
const btnabout = document.querySelector('#btn-about-us');
new WOW().init();

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}
/* 
// Variables Slider


let posicionActual = 0;
let $botonRetroceder = document.querySelector('#retroceder');
let $botonAvanzar = document.querySelector('#avanzar');


// Funciones 
/**
 * Funcion que cambia la foto en la siguiente posicion

function pasarFoto() {
    IMAGENES[posicionActual].style.opacity = '.05';
    if (posicionActual >= IMAGENES.length - 1) {
        posicionActual = 0;
        IMAGENES[posicionActual].style.opacity = '1';
        IMAGENES[posicionActual].style.display = 'block';
    } else {

        posicionActual++;
    }
    renderizarImagen();
}
/**
 * Funcion que cambia la foto en la anterior posicion

function retrocederFoto() {
    IMAGENES[posicionActual].style.opacity = '.05';
    console.log(posicionActual);
    if (posicionActual <= 0) {
        posicionActual = IMAGENES.length - 1;
        console.log(posicionActual);
        IMAGENES[posicionActual].style.opacity = '1';
        IMAGENES[posicionActual].style.display = 'block';
    } else {

        console.log(posicionActual);
        posicionActual--;
        console.log(posicionActual);
    }
    renderizarImagen();
}
/**
 * Funcion que actualiza la imagen de imagen dependiendo de posicionActual

function renderizarImagen() {


    console.log(posicionActual);

    if (posicionActual == IMAGENES.length - 1) {
        IMAGENES[0].style.display = 'none';
    } else {
        IMAGENES[posicionActual + 1].style.display = 'none';
        IMAGENES[posicionActual].style.opacity = '1';
        IMAGENES[posicionActual].style.display = 'block';
    }



    if (posicionActual <= 0) {
        IMAGENES[IMAGENES.length - 1].style.display = 'none';
    } else {
        IMAGENES[posicionActual - 1].style.display = 'none';
        IMAGENES[posicionActual].style.opacity = '1';
        IMAGENES[posicionActual].style.display = 'block';
    }



}

 */


/* / Eventos
$botonAvanzar.addEventListener('click', pasarFoto);
$botonRetroceder.addEventListener('click', retrocederFoto);
 */
// Iniciar

btnabout.addEventListener('click', () => {
    window.location = ('nosotros/about-us.html');
}, false);
nav.addEventListener('click', manejoSideBar, false);
// cmyk : gris = #bdbcbc C0 M0 Y0 K35
//azul: # 3299cc C75 M25 Y7 K0