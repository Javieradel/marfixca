const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
const linksWsMulticonexion = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%202Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%203Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%204Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2020Mb',
    ''
];
const linksWsDedicado = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%204Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2030Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2040Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2050Mb'
]
const linksWsGamer = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%204Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20Gamer%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2030Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2040Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2050Mb',

]
const linksWsIP = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2030Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2040Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2050Mb'
]
new WOW().init();

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}

nav.addEventListener('click', manejoSideBar, false);

/*

**************
*/
function enviarWs(obj, arrSms) {

    obj.forEach((elem, indx) => {
        elem.addEventListener('click', (e) => {
            window.location = arrSms[indx];
        })
    })
}


if (document.title == 'Marfix - Planes') {
    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Tengo%20interés%20por%20saber%20más%20de%20sus%20planes%20y%20servicios";
    })


}
if (document.title == 'Marfix - Multiconexión') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsMulticonexion);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20Multiconexión";
    })


}
if (document.title == 'Marfix - Dedicado') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsDedicado);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados";
    })


}
if (document.title == 'Marfix - Dedicado Gamer') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsGamer);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados%20Gamer";
    })


}
if (document.title == 'Marfix - Dedicado IP Pública') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsIP);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados%20con%20IP%20pública";
    })


}