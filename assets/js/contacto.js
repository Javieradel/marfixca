const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
const btnabout = document.querySelector('#btn-about-us');
new WOW().init();

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}
nav.addEventListener('click', manejoSideBar, false);
/**************************** */